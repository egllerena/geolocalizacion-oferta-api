package pe.com.geolocalizacionapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class GeolocalizacionApiApplication extends SpringBootServletInitializer{

	
	public GeolocalizacionApiApplication() {
		super();
		setRegisterErrorPageFilter(false);
	}

	public static void main(String[] args) {
		SpringApplication.run(GeolocalizacionApiApplication.class, args);
	}

}
