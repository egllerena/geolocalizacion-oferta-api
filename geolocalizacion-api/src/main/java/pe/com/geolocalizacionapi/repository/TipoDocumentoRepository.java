package pe.com.geolocalizacionapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pe.com.geolocalizacionapi.entity.TipoDocumentoEntity;

@Repository
public interface TipoDocumentoRepository extends JpaRepository<TipoDocumentoEntity, Long>{
	List<TipoDocumentoEntity> findByActivoTipoDoc(Integer activoTipoDoc);	
}
