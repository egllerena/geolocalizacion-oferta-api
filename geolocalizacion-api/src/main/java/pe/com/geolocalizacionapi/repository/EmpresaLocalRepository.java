package pe.com.geolocalizacionapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pe.com.geolocalizacionapi.entity.EmpresaLocalEntity;

@Repository
public interface EmpresaLocalRepository extends JpaRepository<EmpresaLocalEntity, Long>{

}
