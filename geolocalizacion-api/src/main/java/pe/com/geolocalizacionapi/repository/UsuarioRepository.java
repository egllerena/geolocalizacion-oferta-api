package pe.com.geolocalizacionapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pe.com.geolocalizacionapi.entity.UsuarioEntity;

@Repository
public interface UsuarioRepository extends JpaRepository<UsuarioEntity, Long>{
	UsuarioEntity findByNumeroDocumentoUsuario(String numeroDocumentoUsuario);
	
}
