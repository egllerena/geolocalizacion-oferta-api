package pe.com.geolocalizacionapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pe.com.geolocalizacionapi.entity.OfertaEntity;

@Repository
public interface OfertaRepository extends JpaRepository<OfertaEntity, Long>{

}
