package pe.com.geolocalizacionapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pe.com.geolocalizacionapi.entity.OfertaEntity;
import pe.com.geolocalizacionapi.entity.OfertaLocalEntity;

@Repository
public interface OfertaLocalRepository extends JpaRepository<OfertaLocalEntity, Long>{
	List<OfertaLocalEntity> findByOferta(OfertaEntity oferta);	
}
