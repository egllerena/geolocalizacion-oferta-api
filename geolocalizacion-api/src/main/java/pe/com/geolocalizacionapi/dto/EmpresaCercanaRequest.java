package pe.com.geolocalizacionapi.dto;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EmpresaCercanaRequest {
	private String numeroDocumento;
	private BigDecimal latitud;
	private BigDecimal longitud;
	private Integer pagina;
}
