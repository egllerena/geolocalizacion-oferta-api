package pe.com.geolocalizacionapi.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OfertaCercanaResponse {
	private Long idOferta;
	private String ofertaDescripcion;
	private String ofertaImagen;
	private String validoDesdeOferta;
	private String validoHastaOferta;	
	private Integer nuevoOferta;
	private Integer diasRestantes;
	private List<LocalCercanoResponse> listaLocales;
}
