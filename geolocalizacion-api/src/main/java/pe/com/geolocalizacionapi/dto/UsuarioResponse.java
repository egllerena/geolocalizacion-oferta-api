package pe.com.geolocalizacionapi.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UsuarioResponse {
	private Long idUsuario;
	private String numeroDocumentoUsuario;
	private String nombresUsu;
	private String apellidoPatUsuario;
	private String apellidoMatUsuario;
}
