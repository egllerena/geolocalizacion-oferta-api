package pe.com.geolocalizacionapi.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EmpresaCercanaResponse {
	private Long idEmpresa;
	private String nombreEmpresa;
	private String direccionEmpresa;
	private String imagenEmpresa;
}
