package pe.com.geolocalizacionapi.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JwtResponse {
	private String mensaje;
	private String token;
	private int status;
	private UsuarioResponse user;
}
