package pe.com.geolocalizacionapi.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LocalCercanoResponse {
	private Long idLocal;
	private String nombreLocal;
	private String referenciaLocal;
	private String direccionLocal;
}
