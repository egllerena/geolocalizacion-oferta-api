package pe.com.geolocalizacionapi.dto;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LocalOfertaRequest {
	private String numeroDocumento;
	private Long idEmpresa;
	private Long idOferta;
	private BigDecimal latitud;
	private BigDecimal longitud;
	private Integer pagina;
}
