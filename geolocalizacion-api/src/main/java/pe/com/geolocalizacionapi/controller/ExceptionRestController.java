package pe.com.geolocalizacionapi.controller;

import javax.persistence.EntityNotFoundException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import pe.com.geolocalizacionapi.dto.RespuestaBase;
import pe.com.geolocalizacionapi.enums.ErrorCode;

@RestControllerAdvice
public class ExceptionRestController {
    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<RespuestaBase> handleEntityNotFoundException() {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(RespuestaBase.error(ErrorCode.ENTITY_NOT_FOUND));
    }
    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<RespuestaBase> handleNullPointerException() {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(RespuestaBase.error(ErrorCode.NULL_EXCEPTION));
    }
}