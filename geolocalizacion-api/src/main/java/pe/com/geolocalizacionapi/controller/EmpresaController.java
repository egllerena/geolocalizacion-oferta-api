package pe.com.geolocalizacionapi.controller;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.com.geolocalizacionapi.dto.EmpresaCercanaRequest;
import pe.com.geolocalizacionapi.dto.EmpresaCercanaResponse;
import pe.com.geolocalizacionapi.dto.RespuestaBase;
import pe.com.geolocalizacionapi.service.EmpresaService;

@CrossOrigin(origins="*",maxAge = 3600)
@RestController
@RequestMapping("/api/empresa")
public class EmpresaController {
	
	@Autowired
	private EmpresaService empresaService;
	
	@PostMapping("/getEmpresasCercanas")
	public ResponseEntity<RespuestaBase<List<EmpresaCercanaResponse>>> getEmpresasCercanas(@RequestBody EmpresaCercanaRequest request) {
		if(Objects.equals(request.getNumeroDocumento(), null) || Objects.equals(request.getLatitud(), null) || Objects.equals(request.getLongitud(), null)) {
			throw new NullPointerException();
		} else {
			return ResponseEntity.ok(RespuestaBase.successWithData(empresaService.getEmpresasCercanas(request)));
		}
	}
}
