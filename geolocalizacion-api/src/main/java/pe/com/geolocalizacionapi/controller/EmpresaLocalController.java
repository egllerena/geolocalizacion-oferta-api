package pe.com.geolocalizacionapi.controller;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.com.geolocalizacionapi.dto.LocalCercanoResponse;
import pe.com.geolocalizacionapi.dto.LocalOfertaRequest;
import pe.com.geolocalizacionapi.dto.RespuestaBase;
import pe.com.geolocalizacionapi.service.EmpresaLocalService;

@CrossOrigin(origins="*",maxAge = 3600)
@RestController
@RequestMapping("/api/local")
public class EmpresaLocalController {
	@Autowired
	private EmpresaLocalService empresaLocalService;
	
	@PostMapping("/getLocalesCercanos")
	public ResponseEntity<RespuestaBase<List<LocalCercanoResponse>>> getLocalesCercanos(@RequestBody LocalOfertaRequest request) {
		if(Objects.equals(request.getIdEmpresa(), null) || Objects.equals(request.getNumeroDocumento(), null) || Objects.equals(request.getLatitud(), null) || Objects.equals(request.getLongitud(), null)) {
			throw new NullPointerException();
		} else {
			return ResponseEntity.ok(RespuestaBase.successWithData(empresaLocalService.getLocalesCercanos(request)));
		}
	}
}
