package pe.com.geolocalizacionapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.com.geolocalizacionapi.dto.RespuestaBase;
import pe.com.geolocalizacionapi.entity.TipoDocumentoEntity;
import pe.com.geolocalizacionapi.service.TipoDocumentoService;

@CrossOrigin(origins="*",maxAge = 3600)
@RestController
@RequestMapping("/api/tipodocumento")
public class TipoDocumentoController {
	
	@Autowired
	private TipoDocumentoService tipoDocumentoService;
	
	@GetMapping("/getTiposDocumento")
	public ResponseEntity<RespuestaBase<List<TipoDocumentoEntity>>> getTiposDocumento() {
		return ResponseEntity.ok(RespuestaBase.successWithData(tipoDocumentoService.getTiposDocumento()));
	}
}
