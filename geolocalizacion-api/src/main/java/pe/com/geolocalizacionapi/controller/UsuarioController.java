package pe.com.geolocalizacionapi.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.com.geolocalizacionapi.dto.JwtResponse;
import pe.com.geolocalizacionapi.dto.LoginDto;

@CrossOrigin(origins="*",maxAge = 3600)
@RestController
@RequestMapping("/api/usuario")
public class UsuarioController {
	
	@PostMapping("/login")
	public ResponseEntity<JwtResponse> login(@RequestBody LoginDto login) {
		return ResponseEntity.ok(new JwtResponse());
	}
}
