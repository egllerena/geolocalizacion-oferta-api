package pe.com.geolocalizacionapi.controller;

import java.util.List;
import java.util.Objects;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.com.geolocalizacionapi.dto.LocalOfertaRequest;
import pe.com.geolocalizacionapi.dto.OfertaCercanaRequest;
import pe.com.geolocalizacionapi.dto.OfertaCercanaResponse;
import pe.com.geolocalizacionapi.dto.RespuestaBase;
import pe.com.geolocalizacionapi.service.OfertaService;

@CrossOrigin(origins="*",maxAge = 3600)
@RestController
@RequestMapping("/api/oferta")
public class OfertaController {
	
	@Autowired
	private OfertaService ofertaService;
	
	@PostMapping("/getOfertasCercanas")
	public ResponseEntity<RespuestaBase<List<OfertaCercanaResponse>>> getOfertasCercanas(@RequestBody OfertaCercanaRequest request) {
		if(Objects.equals(request.getNumeroDocumento(), null) || Objects.equals(request.getLatitud(), null) || Objects.equals(request.getLongitud(), null)) {
			throw new NullPointerException();
		} else {
			return ResponseEntity.ok(RespuestaBase.successWithData(ofertaService.getOfertasCercanas(request)));
		}
	}
	
	@PostMapping("/getOferta")
	public ResponseEntity<RespuestaBase<OfertaCercanaResponse>> getOferta(@RequestBody LocalOfertaRequest request) {
		if(Objects.equals(request.getIdOferta(), null) || Objects.equals(request.getNumeroDocumento(), null) ||  Objects.equals(request.getLatitud(), null) || Objects.equals(request.getLongitud(), null)) {
			throw new NullPointerException();
		} else {
			OfertaCercanaResponse oferta = ofertaService.getOferta(request);
			if(Objects.equals(oferta, null)) {
				throw new EntityNotFoundException();
			} else {
				return ResponseEntity.ok(RespuestaBase.successWithData(oferta));
			}
		}
	}
}
