package pe.com.geolocalizacionapi.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name="go_empresa", schema ="geo_oferta")
public class EmpresaEntity {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="go_emp_id")
	private Long idEmpresa;
	
	@Column(name="go_emp_nombre")	
	private String nombreEmpresa;
	
	@Column(name="go_emp_direccion")	
	private String direccionEmpresa;
	
	@Column(name="go_emp_image")	
	private String imagenEmpresa;
		
	@Column(name="go_emp_activo")	
	private Integer activoEmpresa;
	
	@Column(name="go_emp_usu_creacion")	
	private String usuCreacionEmpresa;
	
	@Column(name="go_emp_fecha_creacion")	
	private Date fechaCreacionEmpresa;
}
