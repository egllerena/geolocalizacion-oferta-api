package pe.com.geolocalizacionapi.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name="go_oferta", schema ="geo_oferta")
public class OfertaEntity {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="go_oferta_id")
	private Long idOferta;
	
	@JoinColumn(name="go_categ_id")
	@ManyToOne(optional=true, fetch=FetchType.EAGER)
	private CategoriaEntity categoria;	
	
	@Column(name="go_oferta_descripcion")	
	private String descripcionOferta;
	
	@Column(name="go_oferta_imagen")	
	private String imagenOferta;
	
	@Column(name="go_oferta_valido_desde")	
	private Date validoDesdeOferta;
	
	@Column(name="go_oferta_valido_hasta")	
	private Date validoHastaOferta;	
	
	@Column(name="go_oferta_nuevo")	
	private Integer nuevoOferta;
	
	@Column(name="go_oferta_activo")	
	private Integer activoOferta;
	
	@Column(name="go_oferta_usu_creacion")	
	private String usuCreacionOferta;
	
	@Column(name="go_oferta_fecha_creacion")	
	private Date fechaCreacionOferta;
}
