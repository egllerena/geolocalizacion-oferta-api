package pe.com.geolocalizacionapi.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name="go_usuario", schema ="geo_oferta")
public class UsuarioEntity {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="go_usu_id")
	private Long idUsuario;
	
	@JoinColumn(name="go_tipodoc_id")
	@ManyToOne(optional=true, fetch=FetchType.EAGER)
	private TipoDocumentoEntity tipoDocumento;
	
	@JoinColumn(name="go_categ_id")
	@ManyToOne(optional=true, fetch=FetchType.EAGER)
	private CategoriaEntity categoria;
	
	@Column(name="go_usu_numero_documento")	
	private String numeroDocumentoUsuario;
	
	@Column(name="go_usu_nombres")	
	private String nombresUsu;
	
	@Column(name="go_usu_apellido_pat")	
	private String apellidoPatUsuario;
	
	@Column(name="go_usu_apellido_mat")	
	private String apellidoMatUsuario;
	
	@Column(name="go_usu_activo")	
	private Integer activoUsuario;
	
	@Column(name="go_usu_usu_creacion")	
	private String usuCreacionUsuario;
	
	@Column(name="go_usu_fecha_creacion")	
	private Date fechaCreacionUsuario;	
}
