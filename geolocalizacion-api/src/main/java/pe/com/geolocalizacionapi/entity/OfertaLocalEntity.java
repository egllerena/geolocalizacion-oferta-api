package pe.com.geolocalizacionapi.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name="go_oferta_local", schema ="geo_oferta")
public class OfertaLocalEntity {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="go_ofertloc_id")
	private Long idOfertLoc;
	
	@JoinColumn(name="go_emp_local_id")
	@ManyToOne(optional=true, fetch=FetchType.EAGER)
	private EmpresaLocalEntity empresaLocal;
	
	@JoinColumn(name="go_oferta_id")
	@ManyToOne(optional=true, fetch=FetchType.EAGER)
	private OfertaEntity oferta;	
	
	@Column(name="go_ofertloc_activo")	
	private Integer activoOfertLoc;
	
	@Column(name="go_ofertloc_usu_creacion")	
	private String usuCreacionOfertLoc;
	
	@Column(name="go_ofertloc_fecha_creacion")	
	private Date fechaCreacionOfertLoc;
}
