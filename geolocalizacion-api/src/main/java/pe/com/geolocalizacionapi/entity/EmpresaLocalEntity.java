package pe.com.geolocalizacionapi.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name="go_emp_local", schema ="geo_oferta")
public class EmpresaLocalEntity {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="go_emp_local_id")
	private Long idEmpLocal;
	
	@JoinColumn(name="go_emp_id")
	@ManyToOne(optional=true, fetch=FetchType.EAGER)
	private EmpresaEntity empresa;
		
	@Column(name="go_emp_local_nombre")	
	private String nombreEmpLocal;
	
	@Column(name="go_emp_local_direccion")	
	private String direccionEmpLocal;
	
	@Column(name="go_emp_local_referencia")	
	private String referenciaEmpLocal;
	
	@Column(name="go_emp_local_latitude")	
	private BigDecimal latitudeEmpLocal;
	
	@Column(name="go_emp_local_longitude")	
	private BigDecimal longitudeEmpLocal;	
	
	@Column(name="go_emp_local_activo")	
	private Integer activoEmpLocal;
	
	@Column(name="go_emp_local_usu_creacion")	
	private String usuCreacionEmpLocal;
	
	@Column(name="go_emp_local_fecha_creacion")	
	private Date fechaCreacionEmpLocal;
}
