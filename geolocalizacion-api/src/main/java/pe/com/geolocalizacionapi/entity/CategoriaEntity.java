package pe.com.geolocalizacionapi.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name="go_categoria", schema ="geo_oferta")
public class CategoriaEntity {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="go_categ_id")
	private Long idCategoria;
	
	@Column(name="go_categ_nombre")
	private String nombreCategoria;
	
	@Column(name="go_categ_activo")
	private Integer activoCategoria;
	
	@Column(name="go_categ_usu_creacion")
	private String usuCreacionCategoria;
	
	@Column(name="go_categ_fecha_creacion")
	private Date fechaCreacionCategoria;
}
