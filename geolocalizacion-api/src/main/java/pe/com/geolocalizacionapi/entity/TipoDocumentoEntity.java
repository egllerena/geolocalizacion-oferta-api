package pe.com.geolocalizacionapi.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name="go_tipo_documento", schema ="geo_oferta")
public class TipoDocumentoEntity {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="go_tipodoc_id")
	private Long idTipoDocumento;
	
	@Column(name="go_tipodoc_nombre")
	private String nombreTipoDoc;
	
	@Column(name="go_tipodoc_activo")
	private Integer activoTipoDoc;
	
	@Column(name="go_tipodoc_usu_creacion")
	private String usuCreacionTipoDoc;
	
	@Column(name="go_tipodoc_fecha_creacion")
	private Date fechaCreacionTipoDoc;
}
