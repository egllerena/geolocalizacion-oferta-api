package pe.com.geolocalizacionapi.service;

import java.util.List;

import pe.com.geolocalizacionapi.dto.LocalOfertaRequest;
import pe.com.geolocalizacionapi.dto.OfertaCercanaRequest;
import pe.com.geolocalizacionapi.dto.OfertaCercanaResponse;

public interface OfertaService {
	List<OfertaCercanaResponse> getOfertasCercanas(OfertaCercanaRequest request);
	OfertaCercanaResponse getOferta(LocalOfertaRequest request);
}
