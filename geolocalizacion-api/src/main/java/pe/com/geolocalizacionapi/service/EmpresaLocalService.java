package pe.com.geolocalizacionapi.service;

import java.util.List;

import pe.com.geolocalizacionapi.dto.LocalCercanoResponse;
import pe.com.geolocalizacionapi.dto.LocalOfertaRequest;

public interface EmpresaLocalService {
	List<LocalCercanoResponse> getLocalesCercanos(LocalOfertaRequest request);
}
