package pe.com.geolocalizacionapi.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.com.geolocalizacionapi.entity.UsuarioEntity;
import pe.com.geolocalizacionapi.repository.UsuarioRepository;
import pe.com.geolocalizacionapi.service.JpaUserDetailService;

@Service
public class JpaUserDetailServiceImpl implements JpaUserDetailService {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	private Logger logger = LoggerFactory.getLogger(JpaUserDetailServiceImpl.class);
		
	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UsuarioEntity usuario = usuarioRepository.findByNumeroDocumentoUsuario(username);
		String usuarioCorrecto = "";
		if(usuario == null) {
			String usuarioNoBBVA = "DEFAULT";
			UsuarioEntity usuarioDefault = usuarioRepository.findByNumeroDocumentoUsuario(usuarioNoBBVA);
			if(usuarioDefault == null) {
				logger.error("No existe");
				throw new UsernameNotFoundException(username + "No existe en el sistema.");
			} else {
				usuarioCorrecto = usuarioDefault.getNumeroDocumentoUsuario();
			}
		} else {
			usuarioCorrecto = usuario.getNumeroDocumentoUsuario();
		}
				
		List<GrantedAuthority> role_name = new ArrayList<>();
		String password = "$2a$10$Qh/C5BFSC6NGPrZhOqQQFej0WUTbT5zfc0PCOhLSkGng/EswyDl2C";
		
		
		return new User(usuarioCorrecto, password, true, true, true, true, role_name);
	}

}
