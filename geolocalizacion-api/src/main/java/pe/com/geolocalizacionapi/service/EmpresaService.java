package pe.com.geolocalizacionapi.service;

import java.util.List;

import pe.com.geolocalizacionapi.dto.EmpresaCercanaRequest;
import pe.com.geolocalizacionapi.dto.EmpresaCercanaResponse;

public interface EmpresaService {
	List<EmpresaCercanaResponse> getEmpresasCercanas(EmpresaCercanaRequest request);
}
