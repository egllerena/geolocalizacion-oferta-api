package pe.com.geolocalizacionapi.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.springframework.stereotype.Service;

import pe.com.geolocalizacionapi.dto.EmpresaCercanaRequest;
import pe.com.geolocalizacionapi.dto.EmpresaCercanaResponse;
import pe.com.geolocalizacionapi.service.EmpresaService;

@Service
public class EmpresaServiceImpl implements EmpresaService {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<EmpresaCercanaResponse> getEmpresasCercanas(EmpresaCercanaRequest request) {
		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("geo_oferta.sp_getempresascercanas")
				.registerStoredProcedureParameter(1, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(2, Double.class, ParameterMode.IN)
				.registerStoredProcedureParameter(3, Double.class, ParameterMode.IN)
				.registerStoredProcedureParameter(4, Integer.class, ParameterMode.IN)
				.setParameter(1, request.getNumeroDocumento())
				.setParameter(2, Double.valueOf(request.getLatitud().doubleValue()))
				.setParameter(3, Double.valueOf(request.getLongitud().doubleValue()))
				.setParameter(4, request.getPagina());
		query.execute();
		
		List<Object[]> rows = query.getResultList();
		
		List<EmpresaCercanaResponse> listaEmpresasCercanas = new ArrayList<>();
		
		for (Object[] object : rows) {
			EmpresaCercanaResponse empresa = EmpresaCercanaResponse.builder()
					.idEmpresa(Objects.equals(object[0], null) ? null:Long.valueOf(object[0].toString()))
					.nombreEmpresa(Objects.equals(object[1], null) ? null:object[1].toString())
					.direccionEmpresa(Objects.equals(object[2], null) ? null:object[2].toString())
					.imagenEmpresa(Objects.equals(object[3], null) ? null:object[3].toString())
					.build();
			
			listaEmpresasCercanas.add(empresa);
		}
		return listaEmpresasCercanas;
	}
}
