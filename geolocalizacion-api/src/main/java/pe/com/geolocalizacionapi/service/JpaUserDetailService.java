package pe.com.geolocalizacionapi.service;

import org.springframework.security.core.userdetails.UserDetailsService;

public interface JpaUserDetailService extends UserDetailsService{

}
