package pe.com.geolocalizacionapi.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.com.geolocalizacionapi.auth.constantes.ConstantesAuth;
import pe.com.geolocalizacionapi.entity.TipoDocumentoEntity;
import pe.com.geolocalizacionapi.repository.TipoDocumentoRepository;
import pe.com.geolocalizacionapi.service.TipoDocumentoService;

@Service
public class TipoDocumentoServiceImpl implements TipoDocumentoService {
	
	@Autowired
	private TipoDocumentoRepository tipoDocumentoRepository;
	
	@Override
	public List<TipoDocumentoEntity> getTiposDocumento() {
		return tipoDocumentoRepository.findByActivoTipoDoc(ConstantesAuth.REGISTRO_ACTIVO);
	}

}
