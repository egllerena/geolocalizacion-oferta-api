package pe.com.geolocalizacionapi.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.springframework.stereotype.Service;

import pe.com.geolocalizacionapi.dto.LocalCercanoResponse;
import pe.com.geolocalizacionapi.dto.LocalOfertaRequest;
import pe.com.geolocalizacionapi.service.EmpresaLocalService;

@Service
public class EmpresaLocalServiceImpl implements EmpresaLocalService {
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<LocalCercanoResponse> getLocalesCercanos(LocalOfertaRequest request) {
		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("geo_oferta.sp_getlocalescercanos")
				.registerStoredProcedureParameter(1, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(2, Long.class, ParameterMode.IN)
				.registerStoredProcedureParameter(3, Long.class, ParameterMode.IN)
				.registerStoredProcedureParameter(4, Double.class, ParameterMode.IN)
				.registerStoredProcedureParameter(5, Double.class, ParameterMode.IN)
				.registerStoredProcedureParameter(6, Integer.class, ParameterMode.IN)
				.setParameter(1, request.getNumeroDocumento())
				.setParameter(2, request.getIdEmpresa())
				.setParameter(3, request.getIdOferta())
				.setParameter(4, Double.valueOf(request.getLatitud().doubleValue()))
				.setParameter(5, Double.valueOf(request.getLongitud().doubleValue()))
				.setParameter(6, request.getPagina());
		query.execute();
		
		List<Object[]> rows = query.getResultList();
		
		List<LocalCercanoResponse> listaLocalesCercanos = new ArrayList<>();
		
		for (Object[] object : rows) {
			LocalCercanoResponse oferta = LocalCercanoResponse.builder()
					.idLocal(Objects.equals(object[0], null) ? null:Long.valueOf(object[0].toString()))
					.nombreLocal(Objects.equals(object[1], null) ? null:object[1].toString())
					.referenciaLocal(Objects.equals(object[2], null) ? null:object[2].toString())
					.direccionLocal(Objects.equals(object[3], null) ? null:object[3].toString())
					.build();
			
			listaLocalesCercanos.add(oferta);
		}
		return listaLocalesCercanos;
	}	
}
