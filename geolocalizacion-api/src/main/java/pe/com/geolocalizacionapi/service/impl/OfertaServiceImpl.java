package pe.com.geolocalizacionapi.service.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.com.geolocalizacionapi.auth.constantes.ConstantesAuth;
import pe.com.geolocalizacionapi.dto.LocalOfertaRequest;
import pe.com.geolocalizacionapi.dto.OfertaCercanaRequest;
import pe.com.geolocalizacionapi.dto.OfertaCercanaResponse;
import pe.com.geolocalizacionapi.entity.OfertaEntity;
import pe.com.geolocalizacionapi.repository.OfertaLocalRepository;
import pe.com.geolocalizacionapi.repository.OfertaRepository;
import pe.com.geolocalizacionapi.service.EmpresaLocalService;
import pe.com.geolocalizacionapi.service.OfertaService;

@Service
public class OfertaServiceImpl implements OfertaService {
	
	@PersistenceContext
	private EntityManager entityManager;	
	
	@Autowired
	private OfertaRepository ofertaRepository;
	
	@Autowired
	private OfertaLocalRepository ofertaLocalRepository;
	
	@Autowired
	private EmpresaLocalService empresaLocalService;
	
	public String convertirDateAString(Date fechaDate, String formato) {
		DateFormat fecha = new SimpleDateFormat(formato);
		String convertirString = null;
		convertirString = fecha.format(fechaDate);
		return convertirString;
	}
	
	public Date convertirStringADate(String fechaString) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat(ConstantesAuth.FORMATO_DATE);
		Date convertirDate = formatter.parse(fechaString);
		return convertirDate;
	}

	@Override
	public List<OfertaCercanaResponse> getOfertasCercanas(OfertaCercanaRequest request) {
		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("geo_oferta.sp_getofertascercanas")
				.registerStoredProcedureParameter(1, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(2, Long.class, ParameterMode.IN)
				.registerStoredProcedureParameter(3, Double.class, ParameterMode.IN)
				.registerStoredProcedureParameter(4, Double.class, ParameterMode.IN)
				.registerStoredProcedureParameter(5, Integer.class, ParameterMode.IN)
				.setParameter(1, request.getNumeroDocumento())
				.setParameter(2, request.getIdLocal())
				.setParameter(3, Double.valueOf(request.getLatitud().doubleValue()))
				.setParameter(4, Double.valueOf(request.getLongitud().doubleValue()))
				.setParameter(5, request.getPagina());
		query.execute();
		
		List<Object[]> rows = query.getResultList();
		
		List<OfertaCercanaResponse> listaOfertasCercanas = new ArrayList<>();
		
		for (Object[] object : rows) {
			OfertaCercanaResponse oferta = OfertaCercanaResponse.builder()
					.idOferta(Objects.equals(object[0], null) ? null:Long.valueOf(object[0].toString()))
					.ofertaDescripcion(Objects.equals(object[1], null) ? null:object[1].toString())
					.ofertaImagen(Objects.equals(object[2], null) ? null:object[2].toString())
					.validoDesdeOferta(Objects.equals(object[3], null) ? null:convertirDateAString((Date) object[3], ConstantesAuth.FORMATO_DATE))
					.validoHastaOferta(Objects.equals(object[4], null) ? null:convertirDateAString((Date) object[4], ConstantesAuth.FORMATO_DATE))
					.nuevoOferta(Objects.equals(object[5], null) ? null:Integer.valueOf(object[5].toString()))
					.diasRestantes(Objects.equals(object[6], null) ? null:Integer.valueOf(object[6].toString()))
					.listaLocales(null)
					.build();
			
			listaOfertasCercanas.add(oferta);
		}
		return listaOfertasCercanas;
	}

	@Override
	public OfertaCercanaResponse getOferta(LocalOfertaRequest request) {
		Optional<OfertaEntity> oferta = ofertaRepository.findById(request.getIdOferta());
		if(oferta.isPresent()) {
			OfertaCercanaResponse ofertaDetalle = new OfertaCercanaResponse();
			ofertaDetalle.setIdOferta(oferta.get().getIdOferta());
			ofertaDetalle.setOfertaDescripcion(oferta.get().getDescripcionOferta());
			ofertaDetalle.setOfertaImagen(oferta.get().getImagenOferta());
			ofertaDetalle.setValidoDesdeOferta(convertirDateAString(oferta.get().getValidoDesdeOferta(), ConstantesAuth.FORMATO_DATE));
			ofertaDetalle.setValidoHastaOferta(convertirDateAString(oferta.get().getValidoHastaOferta(), ConstantesAuth.FORMATO_DATE));
			ofertaDetalle.setNuevoOferta(oferta.get().getNuevoOferta());
			ofertaDetalle.setDiasRestantes(null);
			ofertaDetalle.setListaLocales(empresaLocalService.getLocalesCercanos(request));			
			return ofertaDetalle;
		}		
		return null;		
	}
}
