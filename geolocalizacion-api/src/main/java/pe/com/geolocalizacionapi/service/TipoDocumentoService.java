package pe.com.geolocalizacionapi.service;

import java.util.List;

import pe.com.geolocalizacionapi.entity.TipoDocumentoEntity;

public interface TipoDocumentoService {
	List<TipoDocumentoEntity> getTiposDocumento();
}
