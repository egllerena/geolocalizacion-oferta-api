package pe.com.geolocalizacionapi.auth.service;

import java.io.IOException;

import org.springframework.security.core.Authentication;

import io.jsonwebtoken.Claims;
import pe.com.geolocalizacionapi.entity.UsuarioEntity;

public interface JwtService {
	public String crear(Authentication auth) throws IOException;
	public boolean validar(String token);
	public Claims getClaims(String token);
	public String getUsername(String token);
	public String resolverToken(String token);
	public UsuarioEntity obtenerDataUsuario(String numeroDocumento);
}
