package pe.com.geolocalizacionapi.auth.constantes;

public class ConstantesAuth {
	public static final String CLAVE_SECRETA = "bcpchallenge0910";
	public static final String ATRIBUTO_ROLES = "authorities";
	public static final String TIPO_TOKEN = "Bearer ";
	public static final Long TIEMPO_EXPIRACION = 14000000L;
	public static final String HEADER_AUTH = "Authorization";
	public static final Integer REGISTRO_ACTIVO = 1;
	public static final String FORMATO_DATE = "dd/MM/yyyy";
}
