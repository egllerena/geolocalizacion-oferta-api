package pe.com.geolocalizacionapi.auth.service.impl;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import pe.com.geolocalizacionapi.auth.constantes.ConstantesAuth;
import pe.com.geolocalizacionapi.auth.service.JwtService;
import pe.com.geolocalizacionapi.entity.UsuarioEntity;
import pe.com.geolocalizacionapi.repository.UsuarioRepository;

@Service
public class JwtServiceImpl implements JwtService {
	
	@Autowired
	UsuarioRepository usuarioRepository;

	@Override
	public String crear(Authentication auth) throws IOException {
		String username = ((User)auth.getPrincipal()).getUsername();
		Collection<? extends GrantedAuthority> roles = auth.getAuthorities();
		
		Claims claims = Jwts.claims();
		claims.put(ConstantesAuth.ATRIBUTO_ROLES, new ObjectMapper().writeValueAsString(roles));
		
		String token = Jwts.builder()
				.setClaims(claims)
				.setSubject(username)
				.signWith(SignatureAlgorithm.HS512, ConstantesAuth.CLAVE_SECRETA.getBytes())
				.setIssuedAt(new Date())
				.setExpiration(new Date(System.currentTimeMillis() + Long.valueOf(ConstantesAuth.TIEMPO_EXPIRACION))) //4horas expiración
				.compact();
		return token;
	}

	@Override
	public boolean validar(String token) {		
		// Manejar errores del token
		try {
			getClaims(token);
			return true;
		} catch (JwtException | IllegalArgumentException e) {
			return false;
		}
	}

	@Override
	public Claims getClaims(String token) {
		Claims claimsToken = Jwts.parser()
				.setSigningKey(ConstantesAuth.CLAVE_SECRETA.getBytes())
				.parseClaimsJws(token.replace(ConstantesAuth.TIPO_TOKEN, "")).getBody();
		return claimsToken;
	}

	@Override
	public String getUsername(String token) {
		return getClaims(token).getSubject();
	}
	

	@Override
	public String resolverToken(String token) {
		if(token != null && token.startsWith(ConstantesAuth.TIPO_TOKEN)) {
			return token.replace(ConstantesAuth.TIPO_TOKEN, "");
		}
		return null;
	}

	@Override
	public UsuarioEntity obtenerDataUsuario(String numeroDocumento) {
		UsuarioEntity user = new UsuarioEntity();
		user = usuarioRepository.findByNumeroDocumentoUsuario(numeroDocumento);
		return user;
	}
	
}
