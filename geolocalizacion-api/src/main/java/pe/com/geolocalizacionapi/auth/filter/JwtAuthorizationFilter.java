package pe.com.geolocalizacionapi.auth.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import pe.com.geolocalizacionapi.auth.constantes.ConstantesAuth;
import pe.com.geolocalizacionapi.auth.service.JwtService;

public class JwtAuthorizationFilter extends BasicAuthenticationFilter {
	
	private JwtService jwtService;
	
	public JwtAuthorizationFilter(AuthenticationManager authenticationManager, JwtService jwtService) {
		super(authenticationManager);
		this.jwtService = jwtService;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		String header = request.getHeader(ConstantesAuth.HEADER_AUTH);
		if(!requiresAuthentication(header)) {
			chain.doFilter(request, response);
			return ;
		}
				
		UsernamePasswordAuthenticationToken authentication = null;
		if(jwtService.validar(header)) {
			authentication = new UsernamePasswordAuthenticationToken(jwtService.getUsername(header), null, null);
		} 
		SecurityContextHolder.getContext().setAuthentication(authentication);
		chain.doFilter(request, response);
	}
	
	protected boolean requiresAuthentication(String header) {
		if(header == null || !header.startsWith(ConstantesAuth.TIPO_TOKEN)) {
			return false;
		}
		return true;
	}
	
}
